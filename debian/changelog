ibus-hangul (1.5.3-3) UNRELEASED; urgency=medium

  * debian/compat: Bump the debhelper compat version to 13
  * debian/rules:
    - Remove -Wl,--as-needed linker flag; the bullseye tool chain defaults
      to it

 -- Changwoo Ryu <cwryu@debian.org>  Fri, 01 May 2020 01:57:26 +0900

ibus-hangul (1.5.3-2) unstable; urgency=low

  * Update the apparmor profiles
  * Install binaries in /usr/libexec instead of /usr/lib/ibus
    (Closes: #955220)
    - debian/patches/specify-setup-in-ibus-component.patch: Specify setup
      path in IBus component file
    - debian/rules: Stop using --libexec configure flag
    - debian/apparmor/usr.libexec.*: Renamed from usr.lib.ibus.*
    - debian/ibus-hangul.install: Likewise
    - debian/ibus-hangul.maintscript: Move apparmor profiles
  * debian/control: Bump Standards-Version to 4.5.0
  * debian/copyright: Update copyright years
  * debian/*.bug-script: Use /bin/sh instead of Bash

 -- Changwoo Ryu <cwryu@debian.org>  Sat, 11 Apr 2020 05:12:22 +0900

ibus-hangul (1.5.3-1) unstable; urgency=medium

  * New upstream version
    - debian/patches/fix-exit-on-start-up: Dropped
  * debian/ibus-hangul.bug-script: Update
    - Use gsettings instead of dconf
    - Add GNOME input source settings in case of GNOME
  * debian/apparmor/*: Use apparmor abstractions from libhangul
    0.1.0+git20191003-2
  * debian/control:
    - Add libhangul1 versioned dependency for ctypes-accessed symbols and
      the apparmor abstraction
    - Bump the required ibus version to >= 1.5.20; the upstream source
      requires only >= 1.5.4. But 1.5.20 introduced the client-side preedit
      commit feature which is important for Korean input.
    - Build-Depends on gettext 0.19.8, for desktop entry translation

 -- Changwoo Ryu <cwryu@debian.org>  Wed, 11 Dec 2019 15:30:57 +0900

ibus-hangul (1.5.2-1) unstable; urgency=medium

  * New upstream version
  * debian/ibus-hangul.bug-script: Add bug script
  * debian/compat: Removed
  * debian/control:
    - Use Build-Depends debhelper-compat (= 12) instead of debhelper
    - Add Rules-Requires-Root
    - Standards-Version: 4.4.1
  * debian/apparmor/usr.lib.ibus.ibus-engine-hangul: Update apparmor file
  * debian/patches/fix-exit-on-start-up.patch: Fix upstream breakage; it
    always exits on start up

 -- Changwoo Ryu <cwryu@debian.org>  Thu, 07 Nov 2019 08:49:21 +0900

ibus-hangul (1.5.1+git20190323-1) unstable; urgency=medium

  * New upstream snapshot
  * d/control:
    * Update Vcs-*
    * Update Description
  * d/salsa.ci.yml: Add salsa CI config
  * Install and enable apparmor

 -- Changwoo Ryu <cwryu@debian.org>  Sun, 21 Jul 2019 00:20:16 -0300

ibus-hangul (1.5.1-1) unstable; urgency=medium

  * New upstream version
  * debian/{control,copyright}: Update the upstream URL
  * debian/patches/window-close.patch: Add patch to quit setup on window
    close

 -- Changwoo Ryu <cwryu@debian.org>  Tue, 31 Jul 2018 20:07:30 +0800

ibus-hangul (1.5.0+git20180526-1) unstable; urgency=medium

  * New upstream version
  * debian/control
    - Standards-Version: 4.1.4
    - Update Vcs-* for salsa.d.o
    - Update the maintainer address (Closes: #899542)
  * debian/copyright - Use HTTPS for the copyright format URL
  * debian/{compat,control} - Use debhelper 11

 -- Changwoo Ryu <cwryu@debian.org>  Fri, 01 Jun 2018 15:15:01 +0900

ibus-hangul (1.5.0+git20161231-1) unstable; urgency=medium

  * New upstream version (Closes: #768061)
  * Standards-Version: 4.1.2
  * debian/rules
    - Avoid useless library dependencies
    - Add hardening build option (hardening=+all)
  * patches:
    - hide-auto-reordering.patch: Hide the auto reordering setup
      - This feature is not available in the libhangul git versions.
    - set-app-name.patch: Set the app name explicitly

 -- Changwoo Ryu <cwryu@debian.org>  Fri, 15 Dec 2017 09:01:57 +0900

ibus-hangul (1.5.0-2) unstable; urgency=medium

  * debian/control: Update Uploaders: (Closes: #841801)
  * Build-Depends on python3 instead of python3-dev (Closes: #809455)
  * Build-Depends on dh-python explicitly
  * Update the upstream URLs; Google Code to GitHub
  * Correct debian/copyright format
  * Standards-Version: 3.9.8
  * debian/control: Use the secure URLs for Vcs-* fields

 -- Changwoo Ryu <cwryu@debian.org>  Sun, 06 Nov 2016 18:39:31 +0900

ibus-hangul (1.5.0-1) unstable; urgency=medium

  * New upstream release
  * Use Python 3
  * Standards-Version: 3.9.6
  * debian/watch: Update for the new upstream downloads on github

 -- Changwoo Ryu <cwryu@debian.org>  Mon, 13 Oct 2014 03:32:11 +0900

ibus-hangul (1.4.2+git20140818-1) unstable; urgency=medium

  * New upstream release

 -- Changwoo Ryu <cwryu@debian.org>  Wed, 17 Sep 2014 07:50:57 +0900

ibus-hangul (1.4.2-5) unstable; urgency=medium

  [ Changwoo Ryu ]
  * Update the Hangul mode toggle patch
  * Add Depends on gir1.2-ibus-1-0

 -- Changwoo Ryu <cwryu@debian.org>  Mon, 28 Jul 2014 00:19:18 +0900

ibus-hangul (1.4.2-4) unstable; urgency=medium

  [ Changwoo Ryu ]
  * debian/source/options: Ignore autotools generated files when building
    on the git.
  * Standards-Version: 3.9.5
  * Restore versioned dependency on ibus
  * Allow Hangul mode toggle, from ongoing upstream issue
    https://github.com/choehwanjin/ibus-hangul/pull/18. It helps Hangul
    input on first run GNOME 3.

 -- Changwoo Ryu <cwryu@debian.org>  Mon, 30 Jun 2014 07:46:22 +0900

ibus-hangul (1.4.2-3) unstable; urgency=low

  [ Changwoo Ryu ]
  * debian/copyright: Fix the license name. It should be GPL-2+, not
    GPL2+.

  [ Osamu Aoki ]
  * Apply patch from FC19 to work around case that ibus-dconf sends
    lowercase config section.
  * wrap-and-sort and minor adjustments to control.
  * ibus 1.5 transition build.

 -- Osamu Aoki <osamu@debian.org>  Sat, 14 Sep 2013 14:42:55 +0900

ibus-hangul (1.4.2-2) unstable; urgency=low

  * Team upload.
  * Fix libexecdir to match ibus-setup expectation.
    Closes: #626652, #712576

 -- Osamu Aoki <osamu@debian.org>  Tue, 18 Jun 2013 23:15:59 +0900

ibus-hangul (1.4.2-1) unstable; urgency=low

  [ Changwoo Ryu ]
  * New upstream release
  * Clean ups
    - debian/copyright: Fix the copyright-format URI
    - debian/copyright: Update the copyright year
    - debian/copyright: Fix format
    - debian/control: Remove unnecessary build dependencies
    - debian/rules: Remove unnecessary build dependencies
  * Standards-Version: 3.9.4

 -- Changwoo Ryu <cwryu@debian.org>  Sun, 27 Jan 2013 19:20:29 +0900

ibus-hangul (1.4.1-1) unstable; urgency=low

  [ Changwoo Ryu ]
  * New upstream release
  * Corrected debian/watch; the googlecode redirector is no longer
    available.
  * Standards-Version: 3.9.3
  * Use debhelper 9
  * debian/copyright:
    - Use the copyright format 1.0

 -- Changwoo Ryu <cwryu@debian.org>  Sun, 20 May 2012 00:55:32 +0900

ibus-hangul (1.4.0-1) unstable; urgency=low

  [ Changwoo Ryu ]
  * New upstream release
    - Depends on ibus 1.4.0 to build
    - Obsoletes debian/patches/ibus-1.4.patch
  * debian/ibus-hangul.links:
    - Remove a danling symlink
  * debian/control:
    - Build-Depends on the more recent libhangul (>= 0.1.0)

 -- Changwoo Ryu <cwryu@debian.org>  Tue, 17 Jan 2012 01:27:50 +0900

ibus-hangul (1.3.2-0.1) unstable; urgency=low

  [ Changwoo Ryu ]
  * Non-maintainer upload
  * New upstream release (Closes: #646885)
  * debian/control:
    - Add myself to Uploaders list
  * debian/watch:
    - Corrected using googlecode.debian.net redirector
  * debian/rules:
    - Use debhelper7 style build rules

 -- Changwoo Ryu <cwryu@debian.org>  Sun, 01 Jan 2012 00:25:29 +0900

ibus-hangul (1.3.1-3) unstable; urgency=low

  * merge patches from ubuntu. (closes: #640712, #637385)
  * debian/rules: updated.
  * debian/control:
    - bump standards version to 3.9.2.
    - update Vcs-* fields.
    - depends on ibus >= 1.3.99

 -- LI Daobing <lidaobing@debian.org>  Sat, 01 Oct 2011 21:31:03 +0800

ibus-hangul (1.3.1-2ubuntu2) oneiric; urgency=low

  * Build-depend on libibus-1.0-dev rather than libibus-dev.
  * Apply Fedora patch to follow ibus 1.4 config API change.

 -- Colin Watson <cjwatson@ubuntu.com>  Tue, 23 Aug 2011 11:40:16 +0100

ibus-hangul (1.3.1-2ubuntu1) oneiric; urgency=low

  * Switch to dh_python2. (LP: #788514)

 -- Barry Warsaw <barry@ubuntu.com>  Wed, 10 Aug 2011 15:09:01 -0400

ibus-hangul (1.3.1-2) unstable; urgency=low

  * debian/control:
    - set maintainer to pkg-ime, add me to uploaders.
    - update Vcs-* fields.

 -- LI Daobing <lidaobing@debian.org>  Sun, 27 Feb 2011 17:19:10 +0800

ibus-hangul (1.3.1-1) unstable; urgency=low

  * New upstream release.
  * debian/control:
    - build depends on libhangul-dev (>= 0.0.12)
    - bump standards version to 3.9.1.
  * debian/rules:
    - delete in config.log in clean.
    - move place of hangul_keyboard_list
  * debian/ibus-hangul.links: added.

 -- LI Daobing <lidaobing@debian.org>  Sun, 27 Feb 2011 16:15:45 +0800

ibus-hangul (1.3.0.20100329-1) unstable; urgency=low

  * New upstream release.
  * debian/control:
    - bump standards version to 3.8.4.
    - switch to ibus 1.3.
  * debian/source/format: 3.0.

 -- LI Daobing <lidaobing@debian.org>  Sat, 03 Apr 2010 20:21:15 +0800

ibus-hangul (1.2.0.20100102-1) unstable; urgency=low

  * New upstream release.

 -- LI Daobing <lidaobing@debian.org>  Sun, 03 Jan 2010 12:46:27 +0800

ibus-hangul (1.2.0.20091031-1) unstable; urgency=low

  * new upstream release.
  * debian/control:
    - update Vcs-* field.
    - build depends on libhangul-dev (>= 0.0.10)

 -- LI Daobing <lidaobing@debian.org>  Sun, 01 Nov 2009 18:17:57 +0800

ibus-hangul (1.2.0.20090617-2) unstable; urgency=low

  [ LI Daobing ]
  * debian/control:
    - build-depends: libhangul-dev >= 0.0.9
    - bump standards version to 3.8.3

  [ Loïc Minier ]
  * Drop useless CROSS logic in rules; build uses dh_auto_configure.
  * Drop /usr/share/misc/config.{sub,.guess} conditionals since these are
    always present (thanks to autotools-dev bdep).
  * Pass -s to dh_* in binary-arch.
  * Cleanup rules droppping boilerplate comments and superfluous whitespace.
  * Add XS-Python-Version/XB-Python-Version: fields.
  * Version the python-dev bdep to >= 2.5.
  * Update pot file during build; bdep on intltool.

 -- LI Daobing <lidaobing@debian.org>  Tue, 25 Aug 2009 19:36:06 +0800

ibus-hangul (1.2.0.20090617-1) unstable; urgency=low

  * new upstream release.
  * debian/control:
    - build-depends: libibus-dev >= 1.2
    - depends: ibus >= 1.2
    - bump standards version to 3.8.2

 -- LI Daobing <lidaobing@debian.org>  Fri, 26 Jun 2009 23:46:15 +0800

ibus-hangul (1.1.0.20090328-1) unstable; urgency=low

  * initial release to Debian (closes: #521628)
  * new upstream release.
  * debian/control:
    - build depends on libibus-dev.
    - change maintainer's email.
    - bump standards version to 3.8.1.
    - depends on ${python:Depends}.
  * debian/patches/01_engine.dpatch: no longer needed, removed.
  * no patch needed. remove dpatch from debian/*.
  * debian/rules: install README and AUTHORS

 -- LI Daobing <lidaobing@debian.org>  Thu, 23 Apr 2009 20:13:52 +0800

ibus-hangul (1.1.0.20090328-0ubuntu1) karmic; urgency=low

  * new upstream release (LP: #370199).
  * debian/control:
    - build depends on libibus-dev.
    - change maintainer's email.
    - bump standards version to 3.8.1.
    - depends on ${python:Depends}.
  * debian/patches/01_engine.dpatch: no longer needed, removed.
  * no patch needed. remove dpatch from debian/*.
  * debian/rules: install README and AUTHORS

 -- LI Daobing <lidaobing@debian.org>  Fri, 01 May 2009 12:42:43 +0800

ibus-hangul (0.1.1.20081023-0ubuntu2) jaunty; urgency=low

  * No changes. Rebuild to build with Python 2.6. (LP: #336534)

 -- LI Daobing <lidaobing@gmail.com>  Sat, 14 Mar 2009 22:26:32 +0800

ibus-hangul (0.1.1.20081023-0ubuntu1) jaunty; urgency=low

  * Initial release (LP: #312446)

 -- LI Daobing <lidaobing@gmail.com>  Fri, 13 Feb 2009 20:45:24 +0800
